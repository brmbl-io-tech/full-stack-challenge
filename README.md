# Bramble Engineering: Coding Challenge

Welcome to the Bramble Engineering Full Stack code Challenge.  We are building a world-class SaaS app, and value team members who can do it all from CSS to DevOps and everything in between.  We love to code and are passionate about doing it well.  This is your chance to show the team how you approach problems and give us insight into your abilities.

## Summary

Build a web application that tracks registered users, allows registered users to make JSON API requests, and tracks API usage for each user.

## Rationale

This exercise has two goals:

* It helps us to understand what to expect from you as a developer. How do you write production code, how you reason about App and API design and how you communicate when trying to understand a problem before you solve it.
* It helps you get a feel for what it is like to work at Bramble, as this exercise aims to simulate our day-to-day and expose you to the type of work we are doing here.

We believe this technique is not only better, but also is more fun compared to the whiteboard/quiz interviews so common in the industry.

We are not alone: https://github.com/poteto/hiring-without-whiteboards

We appreciate your time and we look forward to hack on this little project together.

## Objective

The goal is to implement a one-page real-time web app, along with a single JSON API endpoint.

## Requirements

### Web App

The app should provide user registration and authentication mechanisms, and
allow all registered users to see a list of users that includes each of their API request
volumes (integer). No authorization mechanisms are required.

The app should present users in a data table - with sorting, pagination, filtering and basic searching.The app should automatically update the page when a user makes an API request.

### API

The app should provide a JSON API for requesting a list of top 3 users by API
usage. Each request to this endpoint increments that user's API usage count/volume by one (1). The API should use some user authentication mechanism. The API should limit requests.

## Guidance

### Interview process

The interview team is assembled in the slack channel and includes engineers who will be working with you. You are encouraged to chat to them and ask questions about the engineering culture, work and life balance, or anything else that you would like to learn about Bramble.

We understand that the interview is a two-sided process and we’d be happy to answer any questions!

Before writing the actual code, we encourage you to create a some design notes in a README and share it with the team. This document should consist of key trade-offs and key design approaches. Please avoid writing an overly detailed design document. Use this document to make sure the team can provide design feedback and demonstrate that you have investigated the problem space to provide a reasonable design.

Split your code submission using pull requests and give the team an opportunity to review the PRs. A good “rule of thumb” to follow is that the final PR submission is adding a small feature set - it means that the team had an opportunity to contribute the feedback during multiple well defined stages of your work.

Our team will do their best to provide a high quality review of the submitted pull requests in a reasonable time frame. You are spending your time on this. We are going to contribute our time too.

After the final submission, we'll review your code, and if there is a positive result, we may call you for a short pairing/review meeting to discuss your solution and any tradeoffs you made.

Soon after that we'll collect one/two references, arrange a call with the cofounders, and work out the other details. You can start the reference collection process in parallel if you would like to speed up the process, but generally this part is done within just a few days.

After all this, one of our co-founders will send you an offer.

In case of a negative result, our hiring manager will contact you and send a list of the key observations from the team that affected the result. Please don’t be discouraged. Our code review process is focused on the submission, not the candidate, and we will be excited for you to take another challenge at a later time if you feel you have addressed our comments!

### Code and project ownership

This is a test challenge and we have no intent of using the code you have submitted in production. This is your work, and you are free to do whatever you’d like with it.

### Areas of focus

Bramble focuses on simplicity, networking and security, so these are the areas we will be evaluating in the submission:

* Productivity. Use third party libraries and framework(s) where applicable.
* Consistent coding style. E.g. Bramble uses `Mix format` for Elixir code.
* Quality. Please write at least one unit/integration test major components e.g. Users UI, API.
* Concurrency. The system should handle multiple simultaneous users, and provide realtime updates.
* Security. Use proven authentication mechanisms, with robust configurations. Set up the strongest encryption you can.
* Stability. The system should have some basic mechanisms to deal with heavy load.
* Build. Its simple to build and run the project.

### Trade-offs

It is important to write as little code as possible. Otherwise, this task could consume too much time and the overall code quality will suffer.

It is OK and expected if you cut corners. For example, configuration tends to take a lot of time and is not important for this task. So we encourage candidates to use hard codes as much as possible and simply add TODO items showing candidate’s thinking.

For example:

```
// TODO: Add configuration via environment variables and reasonable defaults
// for example http://12factor.net/config
```

Comments like this are really helpful to us because they save you a lot of time and demonstrate to us that you’ve spent time thinking about this problem and provide a clear path to a solution.

Consider making other reasonable trade-offs and make sure you communicate them to the interview team. Here are some other trade-offs that will help you to spend less time on the task:

* Do not implement a system that scales or is highly performing. Instead, it is better to note possible bottlenecks, and communicate the performance improvements you add in the future.
* High availability. It is OK if the system is not highly available. Write down how would you make the system highly available and why your system is not.
* Do not try to achieve full test coverage. This will take too long. Take two key components, e.g. Web UI/API layer and implement one or two test cases that demonstrate your approach to testing.

### Pitfalls and gotchas

To help you out, we’ve composed a list of things that resulted in a no-pass from the interview team:

* Scope creep. Candidates have tried to implement too much and ran out of time and energy. To avoid this pitfall, use the simplest solution that will work. Avoid writing too much code. For example, we’ve seen candidates’ code introduce caching and make mistakes in the caching layer validation logic. Not having caching would have solved this problem.
* Unstructured code. We’ve seen candidates leaving commented chunks of code, having one large file with all the code or not having code structure at all.
* Not communicating. Candidates who submitted all the code to master branch, which does not give us the ability to provide feedback at key checkpoints. Because we are a distributed team, structured communication is critical to us.
* You can skip network security - SSL is not required as we'll only run the project locally.
* You can skip API authentication (e.g. `post "/sign_in"` ) but only registered users can make API requests.
* Implementing custom security algorithms/authentication schemes is usually a bad idea unless you are a trained security researcher/engineer. It is definitely a bad idea for this task - try to stick to industry proven security methods as much as possible.

## Scoring

We want to be as transparent as possible on how we will be scoring your submission. The following table provides a description of different areas you will be evaluated on:

* The submitted code has a clear and modular structure
* The candidate communicated their progress during the interview
* README provides clear instructions
* The candidate outlined the key design points in the README
* The code provides examples of tests covering key components
* The program is working according to the specification
* The candidate demonstrates ability to handle and apply feedback
* Authentication is implemented in a secure way
* The API is implemented in a reasonably secure way
* The code has no obvious problems with running in a cluster of nodes

## Asking questions

It is OK (and encouraged) to ask the interview team questions. Some folks stay away from asking questions to avoid appearing less experienced, so we provided examples of questions to ask and questions we expect candidates to figure out on their own.

This is a great question to ask:

    Is it OK to pre-generate secret data and put the secrets in the repository for the purposes of POC? I will add a note that we will auto-generate secrets in the future.

It demonstrates that you thought about this problem domain, recognize the trade off and save you and the team time by not implementing it.

This is the type of question we expect candidates to figure out on their own:

    What version of Elixir should I use?

We expect candidates to be able to find solutions to common non-project specific questions like this one on their own. Unless specified in the requirements, pick the solution that works best for you.

## Tools

This task should be implemented in Elixir/Phoenix and should work on a 64-bit Linux machine, preferably in Docker.

## Deadline

We'd like you to take no more than 1 week to complete this task, and submit the final MR. To be clear: we don’t expect candidates to code for a whole week! The actual coding time should be closer to 4-8 hours and you have the flexibility to choose when to do the work.

If you find yourself coding more than 8 hours, please get back to us to reduce the scope.

## Communication

When in doubt, always err on the side of over-communicating. We promise that we are not going to subtract any points for seemingly “silly” questions.

Finally THANK YOU for taking your time to take the challenge. We understand that your time is valuable and we really appreciate it!

## Getting Started

First, please create your own repository (DO NOT FORK THIS ONE) - push it to GitLab, and share it with us, working as you would in a professional environment.

Please don't forget to update your README to reflect how to build and run your application.
